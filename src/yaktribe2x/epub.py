from ebooklib import epub
from importlib import resources

from .jinja import env
from .yaktribe import get_gang


AUTHOR = "yaktribe2x"


def _get_css(css):
    return resources.files(__package__).joinpath(f"css/{css}")\
        .read_text()


def create_gang_book(gang):
    book = _create_book(f"roster{gang.id}", gang.name)
    style = epub.EpubItem(uid="style_main",
                          file_name="style/main.css",
                          media_type="text/css",
                          content=_get_css("main.css"))
    book.add_item(style)
    fighters = gang.get_active_fighters()
    template = env.get_template("fighter.html")
    toc = []
    spine = []  # "nav"]
    for fighter in fighters:
        name = fighter["name"]
        file_name = f"{name}.xhtml"
        chap = epub.EpubHtml(title=name, file_name=file_name)
        chap.add_item(style)
        chap.set_content(template.render(**fighter))
        book.add_item(chap)
        toc.append(chap)
        spine.append(chap)
    book.toc = toc
    book.spine = spine
    book.add_item(epub.EpubNcx())
    # book.add_item(epub.EpubNav())
    return book


def render_fighter(fighter):
    template = env.get_template("fighter.html")
    return template.render(**fighter)


def create_palette_book():
    book = _create_book("palette")
    toc = []
    spine = ["nav"]

    # Greyscale
    template = env.get_template("greyscale.html")
    grey_css = epub.EpubItem(uid="style_greyscale",
                             file_name="style/greyscale.css",
                             media_type="text/css",
                             content=_get_css("greyscale.css"))
    book.add_item(grey_css)
    grey = epub.EpubHtml(title="Greyscale", file_name="greyscale.xhtml")
    grey.add_item(grey_css)
    grey.set_content(template.render(colours=_get_greyscale()))
    book.add_item(grey)
    toc.append(grey)
    spine.append(grey)

    # Alignment
    template = env.get_template("alignment.html")
    align = epub.EpubHtml(title="Alignment", file_name="alignment.xhtml")
    align.set_content(template.render())
    book.add_item(align)
    toc.append(align)
    spine.append(align)

    book.toc = toc
    book.spine = spine
    book.add_item(epub.EpubNcx())
    book.add_item(epub.EpubNav())
    return book


def _get_greyscale():
    scale = []
    for i in range(0, 16):
        scale.append(hex(i)[2:] * 6)
    return scale


def _create_book(uid, name=None):
    if not name:
        name = uid.capitalize()
    book = epub.EpubBook()
    book.set_title(name)
    book.set_language("en")
    book.set_identifier(f"{uid}")
    book.add_author(AUTHOR)
    return book


def main(args):
    gang = get_gang(args.gang["id"], args.rules)

    book = create_gang_book(gang)
    epub.write_epub(f"{gang.name}.epub", book)
    # epub.write_epub("palette.epub", epub.create_palette_book())
