## **{{ name }}**
__*{{ type }}* ({{ cost }} Credits)__
{% if wargear %}
**Wargear:** {{ wargear|join(", ") }}
{% endif %}
{% if skills %}
**Skills:** {{ skills|join(", ") }}
{% endif %}
{% if rules %}
**Rules:** {{ rules|join(", ") }}
{% endif %}
{% if notes %}
{{ notes }}
{% endif %}

|   M   |  WS   |  BS   |   S   |   T   |   W   |   I   |   A   |  Ld   |
| :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: |
| {{ m }}"  |  {{ ws }}+   |  {{ bs }}+   |  {{ s }}  |  {{ t }}  |   {{ w }}   |  {{ i }}+   |  {{ a }}  | {{ ld }}+  

|  Cl   |  Wil  |  Int  |  XP   | Kills |
| :---: | :---: | :---: | :---: | :---: |
| {{ cl }}+ | {{ wil }}+ | {{ int }}+ | {{ xp }} | {{ kills }} |

<table>
  <thead>
    <tr>
      <th>Weapons</th>
      <th align="center">S</th>
      <th align="center">L</th>
      <th align="center">S</th>
      <th align="center">L</th>
      <th align="center">Str</th>
      <th align="center">D</th>
      <th align="center">Ap</th>
      <th align="center">Am</th>
    </tr>
  </thead>
  <tbody>
{% for w in weapons %}
    <tr>
      <td rowspan="2"><strong>{{ w.name|abbr|wordwrap(10, wrapstring="<br>")|safe }}</strong></td>
      <td colspan="8">{{ w.traits|join(", ")|replace("*", " * ") }}</td>
    </tr>
    <tr>
      <td align="center">{{ w.r_s }}</td>
      <td align="center">{{ w.r_l }}</td>
      <td align="center">{{ w.a_s }}</td>
      <td align="center">{{ w.a_l }}</td>
      <td align="center">{{ w.s   }}</td>
      <td align="center">{{ w.d   }}</td>
      <td align="center">{{ w.ap  }}</td>
      <td align="center">{{ w.am  }}</td>
    </tr>
{% endfor %}
  </tbody>
</table>

{% if wargear|default(False) %}
{% for gear, desc in wargear|dictsort if desc %}
**{{ gear }}** <sup><small>(wargear)</small></sup>
{{ desc }}

{% endfor %}
{% endif %}
{% if traits|default(False) %}
{% for trait, desc in traits|dictsort if desc %}
**{{ trait }}** <sup><small>(trait)</small></sup>
{{ desc }}

{% endfor %}
{% endif %}
{% if skills|default(False) %}
{% for skill, desc in skills|dictsort if desc %}
**{{ skill }}** <sup><small>(skill)</small></sup>
{{ desc }}

{% endfor %}
{% endif %}
{% if rules|default(False) %}
{% for rule, desc in rules|dictsort if desc %}
**{{ rule }}**
{{ desc }}

{% endfor %}
{% endif %}

