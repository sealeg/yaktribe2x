{% set tactics_list = gang.get_tactics_list() %}
{% if tactics_list %}
{% set dice, tactics = gang.get_tactics_list()|dicelist %}
# Pre-battle
## Tactics

|  {{ dice|upper }}  | Type | Name |
| :----------------: | ---- | ---- |
{% for tactic in tactics %}
| {{ tactic.label }} | {{ tactic.value.type }} | {{ tactic.value.name }} |
{% endfor %}
{% endif %}
---
{% for fighter in fighters %}
- [[#Pre-battle]]
{% for name in gang.get_active_names() %}
- [[#{{ name }}]]
{% endfor %}
- [[#Post-battle]]
---
{{ fighter }}
{% if not loop.last %}
---
{% endif %}
{% endfor %}
---
# Post-battle
## Territories
{% for territory, desc in gang.territories|dictsort %}
### {{ territory }}
{{ desc }}
{% endfor %}

