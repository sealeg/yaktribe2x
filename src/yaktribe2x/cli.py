import argparse
import yaml
import os
from importlib import metadata

from .markdown import main as md_main
from .epub import main as epub_main


def cli():
    version = metadata.version("yaktribe2x")
    cwd = os.getcwd()
    gang_path = os.path.join(cwd, "gang.yaml")
    rules_path = os.path.join(cwd, "rules.yaml")

    parser = argparse.ArgumentParser(
        epilog=f"v{version}",
        description="""
            yaktribe2x pulls your gang data from https://yaktribe.games and
            presents it a number of formats.
        """)
    parser.add_argument(
        "--gang",
        default=gang_path,
        help=f"Gang config file in yaml format (default: {gang_path})")
    parser.add_argument(
        "--rules",
        default=rules_path,
        help=f"Rules file in yaml format (default: {rules_path})")

    subparsers = parser.add_subparsers(
        title="Output Formats",
        required=True)

    md = subparsers.add_parser("markdown", aliases=["md"])
    md.set_defaults(func=md_main)

    epub = subparsers.add_parser("epub")
    epub.set_defaults(func=epub_main)

    args = parser.parse_args()

    with open(args.gang, "r") as gang_yaml:
        args.gang = yaml.safe_load(gang_yaml)
    with open(args.rules, "r") as rules_yaml:
        args.rules = yaml.safe_load(rules_yaml)

    args.func(args)
    return 0
