from jinja2 import Environment, PackageLoader, select_autoescape
from bisect import bisect_left
import re


ABBRS = {
    "Ironhead": "IH",
}
ABBR_RE = re.compile(r"\b.*?\b")

DICE_BY_COUNT = {
    2: {
        "label": "d6",
        "labels": ["1-3", "4-6"],
    },
    3: {
        "label": "d6",
        "labels": ["1-2", "3-4", "5-6"],
    },
    4: {
        "label": "d4",
    },
    6: {
        "label": "d6",
    },
    8: {
        "label": "d8",
    },
    10: {
        "label": "d10",
    },
    12: {
        "label": "d12",
    },
    18: {
        "label": "d36",
        "labels": [
            "11", "12", "13", "14", "15", "16",
            "21", "22", "23", "24", "25", "26",
            "31", "32", "33", "34", "35", "36",
        ]
    },
    20: {
        "label": "d20",
    },
    36: {
        "label": "d66",
        "labels": [
            "11", "12", "13", "14", "15", "16",
            "21", "22", "23", "24", "25", "26",
            "31", "32", "33", "34", "35", "36",
            "41", "42", "43", "44", "45", "46",
            "51", "52", "53", "54", "55", "56",
            "61", "62", "63", "64", "65", "66",
        ]
    },
}


def dicelist(results):
    count = len(results)
    dice_counts = sorted(list(DICE_BY_COUNT.keys()))
    dice_results = []
    try:
        dice_count = dice_counts[bisect_left(dice_counts, count)]
    except IndexError:
        dice_count = dice_count[-1]
    dice = DICE_BY_COUNT[dice_count]
    dice_labels = None
    if "labels" in dice:
        dice_labels = dice["labels"]
    for i, result in enumerate(results):
        if dice_labels:
            label = dice_labels[i]
        else:
            label = str(i + 1)
        dice_results.append({
            "label": label,
            "value": result,
        })
    return dice["label"], dice_results


def _replace(match):
    target = match.group()
    return ABBRS.get(target, target)


def abbr(text):
    return re.sub(ABBR_RE, _replace, text)


env = Environment(loader=PackageLoader(__package__),
                  autoescape=select_autoescape(),
                  trim_blocks=True)
env.filters["abbr"] = abbr
env.filters["dicelist"] = dicelist
