from bs4 import BeautifulSoup
import requests
import re


JSON_URL = "https://yaktribe.games/underhive/json/gang/{gang_id}.json"
HTML_URL = "https://yaktribe.games/underhive/print/cards/{gang_id}"
STAT_HEADERS = [
    "m",
    "ws",
    "bs",
    "s",
    "t",
    "w",
    "i",
    "a",
    "ld",
    "cl",
    "wil",
    "int",
    "xp",
]
WEAPON_HEADERS = [
    "name",
    "r_s",  # Range, Short
    "r_l",  # Range, Long
    "a_s",  # Accuracy, Short
    "a_l",  # Accuracy, Long
    "s",    # Strength
    "d",    # Damage
    "ap",   # Armour penetration
    "am",   # Ammo roll
    "traits",
]


class YaktribeError(Exception):
    pass


class Gang:
    def __init__(self):
        self.id = ""
        self.name = ""
        self.data = {}
        self.fighters_by_name = {}
        self.active_fighters = []
        # Descriptions
        self.rules  = {}
        self.skills = {}
        self.traits = {}
        self.wargear = {}
        self.territories_desc = {}
        self.territories = {}
        self.tactics = {}

    def get_active_names(self):
        for fighter in self.get_active_fighters():
            yield fighter["name"]

    def get_active_fighters(self):
        if not self.active_fighters:
            self.active_fighters = [
                f for f in self.data["gangers"] if f["status"] == "Alive"
            ]
        return self.active_fighters

    def get_tactics_list(self):
        tactics_list = []
        for ttype, tactics in self.tactics.items():
            for tactic in tactics:
                tactics_list.append({
                    "type": ttype,
                    "name": tactic,
                })
        return sorted(tactics_list, key=lambda t: str(t["type"]) + str(t["name"]))

    def add_descriptions(self, rules=None, skills=None, traits=None,
                         wargear=None, territories=None):
        if rules:
            self.rules.update(rules)
        if skills:
            self.skills.update(skills)
        if traits:
            self.traits.update(traits)
        if wargear:
            self.wargear.update(wargear)
        if territories:
            self.territories_desc.update(territories)

    def parse_json(self, json_data):
        self.data.update(json_data["gang"])
        self.id = self.data["gang_id"]
        self.name = self.data["gang_name"]
        by_name = {}
        for fighter in self.data["gangers"]:
            by_name[fighter["name"]] = fighter
        self.fighters_by_name = by_name
        for territory in self.data["territories"]:
            territory, desc = self._get_desc(territory,
                                             self.territories_desc)
            self.territories[territory] = desc
        for territory in self.data["campaign_territories"]:
            territory, desc = self._get_desc(territory,
                                             self.territories_desc)
            self.territories[territory] = desc

    def parse_html(self, html_data):
        soup = BeautifulSoup(html_data, "html.parser")
        cards = soup.body.div.find_all("div", "gang-ganger-card")
        if cards:
            cards = list(cards)[:-1]  # Last card is gang info.
            for card in cards:
                name = str(card.find("h5").contents[1]).strip()
                tables = card.find_all("table")
                if len(tables) > 2:
                    stats = self._parse_html_statline(tables[0])
                    # TODO: handle no weapons case?
                    wtable = tables[1]
                    weapons, traits = self._parse_weapon_table(wtable)
                    wsrtable = tables[2]
                    wargear, skills, rules = self._parse_wsr(wsrtable)
                    fighter = {
                        "weapons": weapons,
                        "traits":  traits,
                        "wargear": wargear,
                        "skills":  skills,
                        "rules":   rules,
                    }
                    fighter.update(stats)
                    try:
                        self.fighters_by_name[name].update(fighter)
                    except KeyError:
                        raise YaktribeError(
                            f"Fighter {name} in HTML but not JSON")

    def _parse_html_statline(self, table):
        if "gang-ganger-stats" in table["class"]:
            row = table.tbody.find_all("tr")[0]
            stats = {}
            for td, header in zip(row.find_all("td"), STAT_HEADERS):
                stats[header] = int(re.sub("[^0-9]", "", td.text))
            return stats
        raise YaktribeError("Statline not found on html Ganger Card")

    def _parse_weapon_table(self, table):
        weapons = []
        traits = {}
        for row in table.find_all("tr"):
            tds = row.find_all("td")
            if tds:
                weapon = {}
                for header, value in zip(WEAPON_HEADERS, tds):
                    value  = str(value.string).strip()
                    if header == "traits":
                        value = value.split(",")
                        for trait in value:
                            trait, desc = self._get_trait_desc(trait)
                            traits[trait] = desc
                    weapon[header] = value
                weapons.append(weapon)
        return weapons, traits

    def _parse_skills_rules(self, table):
        skills = {}
        rules  = {}
        wargear = {}
        rows = table.find_all("tr")
        if len(rows) == 1:
            td = rows[0].find_all("td")[1]
            self._parse_rules_row(td, rules)
        if len(rows) == 2:
            td = rows[0].find_all("td")[1]
            self._parse_skills_row(td, skills)
            td = rows[1].find_all("td")[1]
            self._parse_rules_row(td, rules)
        if len(rows) == 3:
            td = rows[0].find_all("td")[1]
            self._parse_wargear_row(td, wargear)
            td = rows[1].find_all("td")[1]
            self._parse_skills_row(td, skills)
            td = rows[2].find_all("td")[1]
            self._parse_rules_row(td, rules)
        return wargear, skills, rules

    def _parse_wsr(self, table):
        rows = table.find_all("tr")
        wargear = {}
        skills  = {}
        rules   = {}
        for row in rows:
            tds = row.find_all("td")
            label = str(tds[0].string).strip().lower()
            if label == "wargear":
                self._parse_wargear_row(tds[1], wargear)
            elif label == "skills":
                self._parse_skills_row(tds[1], skills)
            elif label == "rules":
                self._parse_rules_row(tds[1], rules)
        return wargear, skills, rules

    def _parse_rules_row(self, td, rules):
        for rule in map(str.strip, str(td.string).split(",")):
            if rule:
                rule, desc = self._get_rule_desc(rule)
                rules[rule] = desc

    def _parse_skills_row(self, td, skills):
        for skill in map(str.strip, str(td.string).split(",")):
            if skill:
                skill, desc = self._get_skill_desc(skill)
                skills[skill] = desc

    def _parse_wargear_row(self, td, wargear):
        for gear in map(str.strip, str(td.string).split(",")):
            if gear:
                gear, desc = self._get_wargear_desc(gear)
                wargear[gear] = desc

    def _get_trait_desc(self, trait):
        return self._get_desc(trait, self.traits)

    def _get_wargear_desc(self, gear):
        return self._get_desc(gear, self.wargear)

    def _get_skill_desc(self, skill):
        return self._get_desc(skill, self.skills)

    def _get_rule_desc(self, rule):
        return self._get_desc(rule, self.rules)

    def _get_desc(self, key, descs):
        try:
            return key, descs[key]
        except KeyError:
            try:
                clean = key[:key.index("(")].strip()
                return clean, descs[clean]
            except (ValueError, KeyError):
                return key, ""


def get_json_data(gang_id):
    url = JSON_URL.format(gang_id=gang_id)
    return requests.get(url).json()


def get_html_data(gang_id):
    url = HTML_URL.format(gang_id=gang_id)
    return requests.get(url).text


def get_gang(config, rules):
    gang_id = config["id"]
    json_data = get_json_data(gang_id)
    html_data = get_html_data(gang_id)
    gang = Gang()
    gang.add_descriptions(**rules)
    gang.parse_json(json_data)
    gang.parse_html(html_data)
    if "tactics" in config:
        gang.tactics = config["tactics"]
    return gang
