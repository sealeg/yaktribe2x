from .jinja import env
from .yaktribe import get_gang


def render_gang(gang):
    fighters = gang.get_active_fighters()
    template = env.get_template("gang.md")
    return template.render(
        gang=gang,
        fighters=map(render_fighter, fighters))


def render_fighter(fighter):
    template = env.get_template("fighter.md")
    return template.render(**fighter)


def main(args):
    gang = get_gang(args.gang, args.rules)
    print(render_gang(gang))
