# yaktribe2x
A little tool to make your Gang info more readable.

## Requirements
Probaby Python 3.8 or above.

## Install
Clone this repo and `cd` into it.

### System Wide

```sh
# Install dependencies:
pip3 install -r requirements.txt

# Install yaktribe2x:
pip3 install .
```

The `yaktribe2x` command should now be available system wide.


### In a Virtual Environment
Don't spam your environment with a bunch dependencies, put it all in a python
venv.

```sh
mkdir venv
python3 -m venv ./venv
./venv/bin/pip3 install -r ./requirements.txt
./venv/bin/pip3 install .
```

You can now run `./venv/bin/yaktribe2x`.


## Configuration
You will need to create a `gang.yaml` configuration file.
Use [gang.example.yaml](gang.example.yaml) as a template.

## Running
`yaktribe2x -h` should tell you everything you need to know.

