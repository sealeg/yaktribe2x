# CHANGELOG



## v0.6.2 (2024-05-26)

### Fix

* fix: gitlab api token biz ([`906419d`](https://gitlab.com/sealeg/yaktribe2x/-/commit/906419da9ecce33e2193ef843a1f06d069edc4c0))


## v0.6.1 (2024-05-26)

### Fix

* fix: disabled auto semvar push ([`4fcb7d9`](https://gitlab.com/sealeg/yaktribe2x/-/commit/4fcb7d9e205ed01f051b46f3dde56f56e9d92309))


## v0.6.0 (2024-05-26)

### Feature

* feat: version cleanup ([`0c98ec6`](https://gitlab.com/sealeg/yaktribe2x/-/commit/0c98ec60acdfb2c5689e0bd0ff4ebc19ab7a5908))


## v0.5.1 (2024-05-26)

### Chore

* chore: ignore history and semantic-release update ([`c9a2050`](https://gitlab.com/sealeg/yaktribe2x/-/commit/c9a2050c5a5d2e2c1efac8dd1c20a1ddfb9f397b))

* chore: cleaned up changelog ([`9d18bb2`](https://gitlab.com/sealeg/yaktribe2x/-/commit/9d18bb2292ef12f4a3f60727294fb6624251e453))

* chore: fixed semantic release config ([`73d001d`](https://gitlab.com/sealeg/yaktribe2x/-/commit/73d001da39ee07df2d2bd105b9c2eda29484ec00))

### Documentation

* docs: readme typo ([`984a282`](https://gitlab.com/sealeg/yaktribe2x/-/commit/984a2827cd37de6ee9eca030c880f769201e690c))

### Feature

* feat: hazard suit rules ([`93f03a9`](https://gitlab.com/sealeg/yaktribe2x/-/commit/93f03a9db3f8bbb1c91e91c0894376618fe55652))

### Fix

* fix(md): weapon table rowspan/colspan weird formatting ([`cb9a915`](https://gitlab.com/sealeg/yaktribe2x/-/commit/cb9a9152873d7be980eb6940a5a34d72d4bc56d9))

### Style

* style(cli): show version in output ([`627aba0`](https://gitlab.com/sealeg/yaktribe2x/-/commit/627aba08bc2bf2870119bb249702b06cd1e529e3))

### Unknown

* pre-commit test ([`6fd3d46`](https://gitlab.com/sealeg/yaktribe2x/-/commit/6fd3d46b29b54502636608b460fa7c0025ef4b49))

* 0.5.1 ([`a1ac9b1`](https://gitlab.com/sealeg/yaktribe2x/-/commit/a1ac9b1a2e6e06ba6eedfcd6148287f06ba3f21b))

* added semvar to dev tools ([`f603768`](https://gitlab.com/sealeg/yaktribe2x/-/commit/f603768132463750a9869428eeebcff274d2d233))

* v0.5.0 ([`6738a8c`](https://gitlab.com/sealeg/yaktribe2x/-/commit/6738a8cd1e916282c7bff006be93e0692723e649))

* poetry ([`9bd3af7`](https://gitlab.com/sealeg/yaktribe2x/-/commit/9bd3af75df05a13719a71fbb4aca0783f391d03c))

* v0.4.0 ([`a957610`](https://gitlab.com/sealeg/yaktribe2x/-/commit/a95761071a90432449c37a4755caaf8cf484e9ce))

* statline from html ([`82f661d`](https://gitlab.com/sealeg/yaktribe2x/-/commit/82f661d8844a2d7544ee2e5f0428eb34d20e8771))

* v0.3.0 ([`cecbb60`](https://gitlab.com/sealeg/yaktribe2x/-/commit/cecbb601fbfcbb14bd7e1fa36628f8b63d238295))

* gang tactics support ([`0667d1a`](https://gitlab.com/sealeg/yaktribe2x/-/commit/0667d1a13787435155ac7a0f8fea72b7149f595e))

* improved rules parsing and fixed issue when a fighter has no rules ([`7e02c07`](https://gitlab.com/sealeg/yaktribe2x/-/commit/7e02c07d3b15b09dd4a50bea5905c134eefd6a72))

* fixed default gang/rules path on Windows ([`65cb920`](https://gitlab.com/sealeg/yaktribe2x/-/commit/65cb920b9820de6e44be72b15b7476ea0daa1e5d))

* init ([`cc2314f`](https://gitlab.com/sealeg/yaktribe2x/-/commit/cc2314f80bdb3fa2ef462c2bcb230dd527d1972b))
