# -*- coding: utf-8 -*-
import pytest  # noqa: F401

from yaktribe2x.jinja import dicelist


@pytest.mark.parametrize("data, expected", [[
    list(range(1, 3)),  # d2
    ("d6", [
        {"label": "1-3", "value": 1},
        {"label": "4-6", "value": 2},
    ])
], [
    list(range(1, 4)),  # d3
    ("d6", [
        {"label": "1-2", "value": 1},
        {"label": "3-4", "value": 2},
        {"label": "5-6", "value": 3},
    ])
], [
    list(range(1, 5)),  # d4
    ("d4", [
        {"label": "1", "value": 1},
        {"label": "2", "value": 2},
        {"label": "3", "value": 3},
        {"label": "4", "value": 4},
    ])
], [
    list(range(1, 6)),  # d6
    ("d6", [
        {"label": "1", "value": 1},
        {"label": "2", "value": 2},
        {"label": "3", "value": 3},
        {"label": "4", "value": 4},
        {"label": "5", "value": 5},
    ])
], [
    list(range(1, 7)),  # d6
    ("d6", [
        {"label": "1", "value": 1},
        {"label": "2", "value": 2},
        {"label": "3", "value": 3},
        {"label": "4", "value": 4},
        {"label": "5", "value": 5},
        {"label": "6", "value": 6},
    ])
]])
def test_dicelist(data, expected):
    assert dicelist(data) == expected
