# -*- coding: utf-8 -*-
import pytest  # noqa: F401
# from unittest import mock  # noqa: F401
import os
import json

from yaktribe2x.yaktribe import Gang
from yaktribe2x.jinja import abbr

from bs4 import BeautifulSoup


TESTS_DIR = os.path.dirname(os.path.realpath(__file__))


@pytest.fixture
def gang():
    return Gang()


@pytest.mark.parametrize("data, expected", [[
    "Ironhead Boltgun",
    "IH Boltgun",
], [
    "Smoke Grenade",
    "Smoke Grenade",
]])
def test_abbr(data, expected):
    assert abbr(data) == expected


def test_Gang(gang):
    with open(f"{TESTS_DIR}/gang.json", "r") as json_file:
        json_data = json.load(json_file)
    with open(f"{TESTS_DIR}/gang.html", "r") as html_file:
        html_data = html_file.read()

    gang.parse_json(json_data)
    gang.parse_html(html_data)
    assert gang.data
    active_fighters = gang.get_active_fighters()
    assert len(active_fighters) == 5
    assert active_fighters[0]["traits"]
    assert active_fighters[4]["weapons"]


@pytest.mark.parametrize("html, expected", [
    [
        """
        <table class="table table-sm mb-1 ">
          <tbody>
            <tr>
              <td class="w-15">SKILLS</td>
              <td>Fearsome, Precision Shot</td>
            </tr>
            <tr>
              <td class="w-15">RULES</td>
              <td>Gang Fighter (Ganger), Gang Hierarchy (Champion)</td>
            </tr>
          </tbody>
        </table>
        """,
        {
            "wargear": [],
            "skills": ["Fearsome", "Precision Shot"],
            "rules":  ["Gang Fighter (Ganger)", "Gang Hierarchy (Champion)"],
        }
    ],
])
def test_parse_wsr(html, expected, gang):
    soup = BeautifulSoup(html, "html.parser")
    table = soup.find_all("table")[0]
    wargear, skills, rules = gang._parse_wsr(table)
    assert expected == {
        "wargear": list(wargear.keys()),
        "skills":  list(skills.keys()),
        "rules":   list(rules.keys()),
    }


@pytest.mark.parametrize("html, expected", [[
    """
    <table class="table gang-ganger-stats table-sm mt-2">
      <thead>
        <tr>
          <th>M</th>
          <th>WS</th>
          <th>BS</th>
          <th>S</th>
          <th>T</th>
          <th>W</th>
          <th>I</th>
          <th>A</th>
          <th class="bg-underhive-light">Ld</th>
          <th class="bg-underhive-light">Cl</th>
          <th class="bg-underhive-light">Wil</th>
          <th class="bg-underhive-light">Int</th>
          <th class="bg-underhive-dark">XP</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>4&quot;</td>
          <td>4+</td>
          <td>3+</td>
          <td>3</td>
          <td>4</td>
          <td>1</td>
          <td>5+</td>
          <td>1</td>
          <td class="bg-underhive-light">7+</td>
          <td class="bg-underhive-light">6+</td>
          <td class="bg-underhive-light">6+</td>
          <td class="bg-underhive-light">7+</td>
          <td class="bg-underhive-dark">4</td>
        </tr>
      </tbody>
    </table>
    """,
    {
        "m":   4,
        "ws":  4,
        "bs":  3,
        "s":   3,
        "t":   4,
        "w":   1,
        "i":   5,
        "a":   1,
        "ld":  7,
        "cl":  6,
        "wil": 6,
        "int": 7,
        "xp":  4,
    }
]])
def test_get_html_statline(html, expected, gang):
    soup = BeautifulSoup(html, "html.parser")
    table = soup.find_all("table", "gang-ganger-stats")[0]
    assert gang._parse_html_statline(table) == expected
